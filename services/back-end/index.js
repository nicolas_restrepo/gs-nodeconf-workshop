const express = require('express')



const router = new express.Router()

router.get('/env', async (req, res)  => {
    res.setHeader('Content-Type', 'application/json')
    res.send(JSON.stringify(process.env))
})

router.get('/data',  (req, res)  => {

    res.setHeader('Content-Type', 'application/json')
    res.json([
            {
                "id" : "978-0641723445",
                "cat" : ["book","hardcover"],
                "name" : "The Lightning Thief",
                "author" : "Rick Riordan",
                "series_t" : "Percy Jackson and the Olympians",
                "sequence_i" : 1,
                "genre_s" : "fantasy",
                "inStock" : true,
                "price" : 12.50,
                "pages_i" : 384
            }
            ,
            {
                "id" : "978-1423103349",
                "cat" : ["book","paperback"],
                "name" : "The Sea of Monsters",
                "author" : "Rick Riordan",
                "series_t" : "Percy Jackson and the Olympians",
                "sequence_i" : 2,
                "genre_s" : "fantasy",
                "inStock" : true,
                "price" : 6.49,
                "pages_i" : 304
            }
            ,
            {
                "id" : "978-1857995879",
                "cat" : ["book","paperback"],
                "name" : "Sophie's World : The Greek Philosophers",
                "author" : "Jostein Gaarder",
                "sequence_i" : 1,
                "genre_s" : "fantasy",
                "inStock" : true,
                "price" : 3.07,
                "pages_i" : 64
            }
            ,
            {
                "id" : "978-1933988177",
                "cat" : ["book","paperback"],
                "name" : "Lucene in Action, Second Edition",
                "author" : "Michael McCandless",
                "sequence_i" : 1,
                "genre_s" : "IT",
                "inStock" : true,
                "price" : 30.50,
                "pages_i" : 475
            }
        ])
})


const server = express()

server.use(router)

server.listen(4001)


